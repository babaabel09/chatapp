import "reflect-metadata";
import { DataSource } from "typeorm";
import { User } from "./entity/User";
import { Message } from "./entity/Message";
require("dotenv").config();

export const AppDataSource = new DataSource({
  type: "mysql",
  host: process.env.DB_HOST,
  port: 3306,
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
  synchronize: true,
  logging: false,
  entities: [Message],
  migrations: [],
  subscribers: [],
});
