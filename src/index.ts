import express from "express";
import { createServer } from "node:http";
import { fileURLToPath } from "node:url";
import { dirname, join } from "node:path";
import { Server } from "socket.io";
import { AppDataSource } from "./data-source";
// import { User } from "./entity/User"
import { Message } from "./entity/Message";
import { MoreThan } from "typeorm";

const app = express();
const server = createServer(app);
const io = new Server(server, {
  connectionStateRecovery: {},
});

AppDataSource.initialize()
  .then(async () => {
    console.log("AppDataSource initialized successfully");
  })
  .catch((error) => console.log(error));

// const __dirname = process.cwd();

app.get("/", (req, res) => {
  res.sendFile(join(__dirname, "index.html"));
});
console.log(__dirname);

io.on("connection", async (socket) => {
  if (!socket.recovered) {
    try {
      const serverOffset = socket.handshake.auth.serverOffset || 0;
      const messages = await AppDataSource.manager.find(Message, {
        where: { id: MoreThan(serverOffset) },
      });

      messages.forEach((message) => {
        socket.emit("chat message", message.content, message.id);
      });
    } catch (e) {
      console.error("Error fetching missed messages:", e);
    }
  }

  socket.on("chat message", async (msg, clientOffset, callback) => {
    try {
      const message = new Message();
      message.client_offset = clientOffset; // Function to generate unique offset
      message.content = msg;
      await AppDataSource.manager.save(message);
      io.emit("chat message", msg, message.id);
      callback();
    } catch (err) {
      if (err.errno === 1062 /* MySQL ER_DUP_ENTRY */) {
        callback();
      } else {
        console.error("MySQL error:", err);
      }
      return;
    }
  });
});

server.listen(3001, () => {
  console.log("server running at http://localhost:3001");
});

function generateUniqueOffset(): string {
  const timestamp = new Date().getTime().toString(36); // Convert timestamp to base36 string
  const randomString = Math.random().toString(36).substring(2, 8); // Generate random string
  return timestamp + randomString; // Concatenate timestamp and random string
}
